import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
matplotlib.use('Agg')
import sklearn
from sklearn.linear_model import LinearRegression
import seaborn as sns; sns.set(style="ticks", color_codes=True)
from scipy.integrate import odeint
from scipy import stats
import sys
import os
from os.path import join
import re
cwd = os.getcwd()
kB = 8.617E-5
T=298.15
# corrections = + ZPE - entropy
#----------------------------------------
#BEEFvdw

C10H21_1_std = np.genfromtxt('1-C10H21_molecule.csv') 
C10H21_2_std = np.genfromtxt('2-C10H21_molecule.csv') 
C10H21_3_std = np.genfromtxt('3-C10H21_molecule.csv') 
C10H21_4_std = np.genfromtxt('4-C10H21_molecule.csv') 
C10H21_5_std = np.genfromtxt('5-C10H21_molecule.csv') 
C10H21O_1_std = np.genfromtxt('1-C10H21O_molecule.csv') 
C10H21O_2_std = np.genfromtxt('2-C10H21O_molecule.csv') 
C10H21O_3_std = np.genfromtxt('3-C10H21O_molecule.csv') 
C10H21O_4_std = np.genfromtxt('4-C10H21O_molecule.csv') 
C10H21O_5_std = np.genfromtxt('5-C10H21O_molecule.csv') 
C10H21O2_1_std = np.genfromtxt('1-C10H21O2_molecule.csv') 
C10H21O2_2_std = np.genfromtxt('2-C10H21O2_molecule.csv') 
C10H21O2_3_std = np.genfromtxt('3-C10H21O2_molecule.csv') 
C10H21O2_4_std = np.genfromtxt('4-C10H21O2_molecule.csv')
C10H21O2_5_std = np.genfromtxt('5-C10H21O2_molecule.csv')
C10H22_std = np.genfromtxt('Decane_molecule.csv') 
C9H19_std = np.genfromtxt('C9H19_molecule.csv') 
CH3_std = np.genfromtxt('CH3_molecule.csv') 
H_std = np.genfromtxt('H_molecule.csv') 
H2_std = np.genfromtxt('H2_molecule.csv') 
H2O_std = np.genfromtxt('H2O_molecule.csv') 
HO2_std = np.genfromtxt('HO2_molecule.csv')
NO_std = np.genfromtxt('NO_molecule.csv') 
NO2_std = np.genfromtxt('NO2_molecule.csv') 
O_std = np.genfromtxt('O_molecule.csv') 
O2_std = np.genfromtxt('O2_molecule.csv') 
OH_std = np.genfromtxt('OH_molecule.csv') 

DFT_energy = {}
for root, dirs, files, in os.walk(cwd):
    for name in dirs:
        try:
            if name == "vtst":
                continue
            os.chdir(join(root, name)) 
            with open('OUTCAR') as f:
                content = f.readlines()
                rcontent = reversed(content)
            for line in rcontent:
                line = line.strip()
                if 'sigma' in line:
                    nums = re.findall(r"[-+]?\d*\.\d+|\d+", line)
                    num = float(nums[-1])
                    DFT_energy[name] = num
                    break
        except:
            pass
C10H21_1 = DFT_energy['1-C10H21'] + 7.7008 - 0.73428
C10H21_2 = DFT_energy['2-C10H21'] + 7.6763 - 0.73945
C10H21_3 = DFT_energy['3-C10H21'] + 7.6739 - 0.74437
C10H21_4 = DFT_energy['4-C10H21'] + 7.6710 - 0.74622
C10H21_5 = DFT_energy['5-C10H21'] + 7.6717 - 0.74555
C10H21O_1 = DFT_energy['1-C10H21O'] + 7.8076 - 0.74898
C10H21O_2 = DFT_energy['2-C10H21O'] + 7.8050 - 0.75144
C10H21O_3 = DFT_energy['3-C10H21O'] + 7.7979 - 0.75644
C10H21O_4 = DFT_energy['4-C10H21O'] + 7.7996 - 0.75193
C10H21O_5 = DFT_energy['5-C10H21O'] + 7.7925 - 0.75205
C10H21O2_1 = DFT_energy['1-C10H21O2'] + 7.9850 - 0.78842
C10H21O2_2 = DFT_energy['2-C10H21O2'] + 7.9630 - 0.78466
C10H21O2_3 = DFT_energy['3-C10H21O2'] + 7.9648 - 0.78131
C10H21O2_4 = DFT_energy['4-C10H21O2'] + 7.9684 - 0.77548
C10H21O2_5 = DFT_energy['5-C10H21O2'] + 7.9689 - 0.77893
C10H22 = DFT_energy['Decane_C10H22'] + 4.3849 - 1.6865
C9H19 = DFT_energy['C9H19'] + 6.9423 - 0.69935
CH3 = DFT_energy['CH3'] + 0.87252 - 0.59999
H = DFT_energy['H'] + 0.001861 - 0.35449
H2 = DFT_energy['H2'] + 0.37539 - 0.40380
H2O = DFT_energy['H2O'] + 0.65466 - 0.58352
HO2 = DFT_energy['HO2'] + 0.42176 - 0.70789
NO = DFT_energy['NO'] + 0.15031 - 0.65125
NO2 = DFT_energy['NO2'] + 0.27182 - 0.74173
O = DFT_energy['O'] + 0.01861 - 0.49768
O2 = DFT_energy['O2'] + 0.12748 - 0.63392
OH = DFT_energy['OH'] + 0.28474 - 0.56767

print('Reaction 1a: ')
rxn1a = (.5*H2) + C10H21_1 - C10H22
rxn1a_std = np.std((.5*H2_std) + C10H21_1_std - C10H22_std)
print(rxn1a)
print(rxn1a_std)

print('Reaction 1b: ')
rxn1b = (0.5*H2) + C10H21_2 - C10H22
rxn1b_std = np.std((0.5*H2_std) + C10H21_2_std - C10H22_std)
print(rxn1b)
print(rxn1b_std)

print('Reaction 1c: ')
rxn1c = (0.5*H2) + C10H21_3 - C10H22
rxn1c_std = np.std((0.5*H2_std) + C10H21_3_std - C10H22_std)
print(rxn1c)
print(rxn1c_std)

print('Reaction 1d: ')
rxn1d = (0.5*H2) + C10H21_4 - C10H22
rxn1d_std = np.std((0.5*H2_std) + C10H21_4_std - C10H22_std)
print(rxn1d)
print(rxn1d_std)

print('Reaction 1e: ')
rxn1e = (0.5*H2) + C10H21_5 - C10H22
rxn1e_std = np.std((0.5*H2_std) + C10H21_5_std - C10H22_std)
print(rxn1e)
print(rxn1e_std)

print('Reaction 2: ')
rxn2 = C9H19 + CH3 - C10H22
rxn2_std = np.std(C9H19_std + CH3_std - C10H22_std)
print(rxn2)
print(rxn2_std)

print('Reaction 3a: ')
rxn3a = HO2 + C10H21_1 - C10H22 - O2
rxn3a_std = np.std(HO2_std + C10H21_1_std - C10H22_std - O2_std)
print(rxn3a)
print(rxn3a_std)

print('Reaction 3b: ')
rxn3b = HO2 + C10H21_2 - C10H22 - O2
rxn3b_std = np.std(HO2_std + C10H21_2_std - C10H22_std - O2_std)
print(rxn3b)
print(rxn3b_std)

print('Reaction 3c: ')
rxn3c = HO2 + C10H21_3 - C10H22 - O2
rxn3c_std = np.std(HO2_std + C10H21_3_std - C10H22_std - O2_std)
print(rxn3c)
print(rxn3c_std)

print('Reaction 3d: ')
rxn3d = HO2 + C10H21_4 - C10H22 - O2
rxn3d_std = np.std(HO2_std + C10H21_4_std - C10H22_std - O2_std)
print(rxn3d)
print(rxn3d_std)

print('Reaction 3e: ')
rxn3e = HO2 + C10H21_5 - C10H22 - O2
rxn3e_std = np.std(HO2_std + C10H21_5_std - C10H22_std - O2_std)
print(rxn3e)
print(rxn3e_std)

print('Reaction 4a: ')
rxn4a = H2 + C10H21_1 - C10H22 - H
rxn4a_std = np.std(H2_std + C10H21_1_std - C10H22_std - H_std)
print(rxn4a)
print(rxn4a_std)

print('Reaction 4b: ')
rxn4b = H2 + C10H21_2 - C10H22 - H
rxn4b_std = np.std(H2_std + C10H21_2_std - C10H22_std - H_std)
print(rxn4b)
print(rxn4b_std)

print('Reaction 4c: ')
rxn4c = H2 + C10H21_3 - C10H22 - H
rxn4c_std = np.std(H2_std + C10H21_3_std - C10H22_std - H_std)
print(rxn4c)
print(rxn4c_std)

print('Reaction 4d: ')
rxn4d = H2 + C10H21_4 - C10H22 - H
rxn4d_std = np.std(H2_std + C10H21_4_std - C10H22_std - H_std)
print(rxn4d)
print(rxn4d_std)

print('Reaction 4e: ')
rxn4e = H2 + C10H21_5 - C10H22 - H
rxn4e_std = np.std(H2_std + C10H21_5_std - C10H22_std - H_std)
print(rxn4e)
print(rxn4e_std)

print('Reaction 5a: ')
rxn5a = H2O + C10H21_1 - C10H22 - OH
rxn5a_std = np.std(H2O_std + C10H21_1_std - C10H22_std - OH_std)
print(rxn5a)
print(rxn5a_std)

print('Reaction 5b: ')
rxn5b = H2O + C10H21_2 - C10H22 - OH
rxn5b_std = np.std(H2O_std + C10H21_2_std - C10H22_std - OH_std)
print(rxn5b)
print(rxn5b_std)

print('Reaction 5c: ')
rxn5c = H2O + C10H21_3 - C10H22 - OH
rxn5c_std = np.std(H2O_std + C10H21_3_std - C10H22_std - OH_std)
print(rxn5c)
print(rxn5c_std)

print('Reaction 5d: ')
rxn5d = H2O + C10H21_4 - C10H22 - OH
rxn5d_std = np.std(H2O_std + C10H21_4_std - C10H22_std - OH_std)
print(rxn5d)
print(rxn5d_std)

print('Reaction 5e: ')
rxn5e = H2O + C10H21_5 - C10H22 - OH
rxn5e_std = np.std(H2O_std + C10H21_5_std - C10H22_std - OH_std)
print(rxn5e)
print(rxn5e_std)

print('Reaction 6a: ')
rxn6a = C10H21O2_1 - C10H21_1 - O2
rxn6a_std = np.std(C10H21O2_1_std - C10H21_1_std - O2_std)
print(rxn6a)
print(rxn6a_std)

print('Reaction 6b: ')
rxn6b = C10H21O2_2 - C10H21_2 - O2
rxn6b_std = np.std(C10H21O2_2_std - C10H21_2_std - O2_std)
print(rxn6b)
print(rxn6b_std)

print('Reaction 6c: ')
rxn6c = C10H21O2_3 - C10H21_3 - O2
rxn6c_std = np.std(C10H21O2_3_std - C10H21_3_std - O2_std)
print(rxn6c)
print(rxn6c_std)

print('Reaction 6d: ')
rxn6d = C10H21O2_4 - C10H21_4 - O2
rxn6d_std = np.std(C10H21O2_4_std - C10H21_4_std - O2_std)
print(rxn6d)
print(rxn6d_std)

print('Reaction 6e: ')
rxn6e = C10H21O2_5 - C10H21_5 - O2
rxn6e_std = np.std(C10H21O2_5_std - C10H21_5_std - O2_std)
print(rxn6e)
print(rxn6e_std)

#can't do rxn7

print('Reaction 8: ')
rxn8 = OH + O - O2 - H
rxn8_std = np.std(OH_std + O_std - O2_std - H_std)
print(rxn8)
print(rxn8_std)

print('Reaction 9: ')
rxn9 = H2 - H - H
rxn9_std = np.std(H2_std - H_std - H_std)
print(rxn9)
print(rxn9_std)

print('Reaction 10: ')
rxn10 = NO2 + OH - HO2 - NO
rxn10_std = np.std(NO2_std + OH_std - HO2_std - NO_std)
print(rxn10)
print(rxn10_std)

print('Reaction 11a: ')
rxn11a = C10H21O_1 + NO2 - C10H21O2_1 - NO
rxn11a_std = np.std(C10H21O_1_std + NO2_std - C10H21O2_1_std - NO_std)
print(rxn11a)
print(rxn11a_std)

print('Reaction 11b: ')
rxn11b = C10H21O_2 + NO2 - C10H21O2_2 - NO
rxn11b_std = np.std(C10H21O_2_std + NO2_std - C10H21O2_2_std - NO_std)
print(rxn11b)
print(rxn11b_std)

print('Reaction 11c: ')
rxn11c = C10H21O_3 + NO2 - C10H21O2_3 - NO
rxn11c_std = np.std(C10H21O_3_std + NO2_std - C10H21O2_3_std - NO_std)
print(rxn11c)
print(rxn11c_std)

print('Reaction 11d: ')
rxn11d = C10H21O_4 + NO2 - C10H21O2_4 - NO
rxn11d_std = np.std(C10H21O_4_std + NO2_std - C10H21O2_4_std - NO_std)
print(rxn11d)
print(rxn11d_std)

print('Reaction 11e: ')
rxn11e = C10H21O_5 + NO2 - C10H21O2_5 - NO
rxn11e_std = np.std(C10H21O_5_std + NO2_std - C10H21O2_5_std - NO_std)
print(rxn11e)
print(rxn11e_std)

print('Reaction 12: ')
rxn12 = NO2 + NO2 - O2 - NO - NO
rxn12_std = np.std(NO2_std + NO2_std - O2_std - NO_std - NO_std)
print(rxn12)
print(rxn12_std)

