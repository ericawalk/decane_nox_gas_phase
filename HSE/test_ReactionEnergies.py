import numpy as np
import sys
import os
import os.path
from os.path import join
import re

cwd = os.getcwd()
T=550+273.15 #Temperature affects entropy corrections
# corrections = + ZPE - entropy*T
#----------------------------------------

#Extract DFT energies
DFT_energy = {}
for root, dirs, files, in os.walk(cwd):
    for name in dirs:
        try:
            if name == "vtst":
                continue
            os.chdir(join(root, name)) 
            with open('OUTCAR') as f:
                content = f.readlines()
                rcontent = reversed(content)
            for line in rcontent:
                line = line.strip()
                if 'sigma' in line:
                    nums = re.findall(r"[-+]?\d*\.\d+|\d+", line)
                    num = float(nums[-1])
                    DFT_energy[name] = num
                    break
        except:
            pass

#Extract DFT standard deviations
DFT_std = {}
for root, dirs, files, in os.walk(cwd):
    for name in dirs:
        try:
            if name == "vtst":
                continue
            os.chdir(join(root, name))
            with open('OUTCAR') as f:
                content = f.readlines()
            output = False
            count=0
            newlines = []
            for line in content:
                line = line.strip()
                if output == True and count <2000:
                    num = float(line)
                    newlines.append(num)
                    count +=1
                if 'ensemble' in line:
                    output = True
                    count = 0
                    newlines = []
            DFT_std[name] = newlines
        except:
            pass
        
#Extract ZPVE and qvib
ZPVE = {}
qvib = {}
for root, dirs, files, in os.walk(cwd):
    try:
        name = str(root)
        name = name[len(cwd)+1:]
        name = name[:-5]
        os.chdir(root)
        if not os.path.isfile('freq.dat'):
            os.system('/projects/academic/mdupuis2/software/vtst/vtsttools/vtstscripts/dymmatrix.pl')
        freq = []
        with open('freq.dat') as f:
            content = f.readlines()
            for line in content:
                line = line.strip()
                nums = re.findall(r"[-+]?\d*\.\d+|\d+", line)
                freq.append(float(nums[0]))
            freq = np.array(freq)
            l = len(freq)
            for k in range(l):
                if freq[k] < 100:
                    freq[k] = 100
            c = 30000000000
            kb = 0.00008617
            h = 0.00000000000000413566
            freq = freq * c
            zpe = .5*h*freq
            ZPVE[name] = np.sum(zpe)
            Qvib = 1/(1-np.exp((-h*freq)/(kb*T)))
            qvib[name] = np.prod(Qvib)
    except:
        pass
os.chdir(cwd)

#Entropy corrections of select molecules (eV)
T0 = 298.15 #(K)
S_T = {}
S_T['C2H4'] = (219.32+40.275/2*(np.log(T)**2-np.log(T0)**2)-185.07*np.log(T/T0))/96.485/1000*T
S_T['C2H6'] = (229.11584+59.505/2*(np.log(T)**2-np.log(T0)**2)-289.16*np.log(T/T0))/96.485/1000*T
S_T['C3H8'] = (269.90984+83.169/2*(np.log(T)**2-np.log(T0)**2)-401.20*np.log(T/T0))/96.485/1000*T
S_T['C4H10'] = (310.32728+105.69/2*(np.log(T)**2-np.log(T0)**2)-504.54*np.log(T/T0))/96.485/1000*T
S_T['C5H12'] = (349.44768+135.73/2*(np.log(T)**2-np.log(T0)**2)-658.96*np.log(T/T0))/96.485/1000*T
S_T['C6H14'] = (388.73544+153.75/2*(np.log(T)**2-np.log(T0)**2)-733.78*np.log(T/T0))/96.485/1000*T
S_T['C7H16'] = (427.98136+175.42/2*(np.log(T)**2-np.log(T0)**2)-833.89*np.log(T/T0))/96.485/1000*T
S_T['C8H18'] = (467.22728+196.39/2*(np.log(T)**2-np.log(T0)**2)-929.85*np.log(T/T0))/96.485/1000*T
S_T['C9H20'] = (506.43136+218.06/2*(np.log(T)**2-np.log(T0)**2)-1030.1*np.log(T/T0))/96.485/1000*T
S_T['C10H22'] = (545.67728+231.99/2*(np.log(T)**2-np.log(T0)**2)-1077.8*np.log(T/T0))/96.485/1000*T
S_T['CH3'] = (28.13786*np.log(T/1000)+36.74736*(T/1000)-4.347218/2*(T/1000)**2-1.595673/3*(T/1000)**3-0.00186/(2*(T/1000)**2)+217.4814)/96.485/1000*T
S_T['H'] = (20.78603*np.log(T/1000)+4.850638E-10*(T/1000)-1.582916E-10/2*(T/1000)**2+1.525102E-11/3*(T/1000)**3-3.196347E-11/(2*(T/1000)**2)+139.8711)/96.485/1000*T
S_T['H2'] = (33.066178*np.log(T/1000)-11.363417*(T/1000)+11.432816/2*(T/1000)**2-2.772874/3*(T/1000)**3+0.158558/(2*(T/1000)**2)+172.707974)/96.485/1000*T
S_T['H2O'] = (30.09200*np.log(T/1000)+6.832514*(T/1000)+6.793435/2*(T/1000)**2-2.534480/3*(T/1000)**3-0.082139/(2*(T/1000)**2)+223.3967)/96.485/1000*T
S_T['HO2'] = (26.00960*np.log(T/1000)+34.85810*(T/1000)-16.30060/2*(T/1000)**2+3.110441/3*(T/1000)**3+0.018611/(2*(T/1000)**2)+250.7660)/96.485/1000*T
S_T['NO'] = (23.83491*np.log(T/1000)+12.58878*(T/1000)-1.139011/2*(T/1000)**2-1.497459/3*(T/1000)**3-0.214194/(2*(T/1000)**2)+237.1219)/96.485/1000*T
S_T['NO2'] = (16.10857*np.log(T/1000)+75.89525*(T/1000)-54.38740/2*(T/1000)**2+14.30777/3*(T/1000)**3-0.239423/(2*(T/1000)**2)+240.5386)/96.485/1000*T
S_T['O2'] = (31.32234*np.log(T/1000)-20.23531*(T/1000)+57.86644/2*(T/1000)**2-36.50624/3*(T/1000)**3+0.007374/(2*(T/1000)**2)+246.7945)/96.485/1000*T
S_T['OH'] = (32.27768*np.log(T/1000)-11.36291*(T/1000)+13.60545/2*(T/1000)**2-3.846486/3*(T/1000)**3+0.001335/(2*(T/1000)**2)+225.5783)/96.485/1000*T
#Total partition function of select molecules
Qtot = {}
for name in S_T:
    Qtot[name] = np.exp(S_T[name]/(kb*T))

#Calculate free energies of species
C10H21_1 = DFT_energy['1-C10H21'] + ZPVE['1-C10H21'] - (kb*T*np.log(qvib['1-C10H21']/qvib['C10H22']*Qtot['C10H22']))
C10H21_2 = DFT_energy['2-C10H21'] + ZPVE['2-C10H21'] - (kb*T*np.log(qvib['2-C10H21']/qvib['C10H22']*Qtot['C10H22']))
C10H21_3 = DFT_energy['3-C10H21'] + ZPVE['3-C10H21'] - (kb*T*np.log(qvib['3-C10H21']/qvib['C10H22']*Qtot['C10H22']))
C10H21_4 = DFT_energy['4-C10H21'] + ZPVE['4-C10H21'] - (kb*T*np.log(qvib['4-C10H21']/qvib['C10H22']*Qtot['C10H22']))
C10H21_5 = DFT_energy['5-C10H21'] + ZPVE['5-C10H21'] - (kb*T*np.log(qvib['5-C10H21']/qvib['C10H22']*Qtot['C10H22']))
C10H21O_1 = DFT_energy['1-C10H21O'] + ZPVE['1-C10H21O'] - (kb*T*np.log(qvib['1-C10H21O']/qvib['C10H22']*Qtot['C10H22']))
C10H21O_2 = DFT_energy['2-C10H21O'] + ZPVE['2-C10H21O'] - (kb*T*np.log(qvib['2-C10H21O']/qvib['C10H22']*Qtot['C10H22']))
C10H21O_3 = DFT_energy['3-C10H21O'] + ZPVE['3-C10H21O'] - (kb*T*np.log(qvib['3-C10H21O']/qvib['C10H22']*Qtot['C10H22']))
C10H21O_4 = DFT_energy['4-C10H21O'] + ZPVE['4-C10H21O'] - (kb*T*np.log(qvib['4-C10H21O']/qvib['C10H22']*Qtot['C10H22']))
C10H21O_5 = DFT_energy['5-C10H21O'] + ZPVE['5-C10H21O'] - (kb*T*np.log(qvib['5-C10H21O']/qvib['C10H22']*Qtot['C10H22']))
C10H21O2_1 = DFT_energy['1-C10H21O2'] + ZPVE['1-C10H21O2'] - (kb*T*np.log(qvib['1-C10H21O2']/qvib['C10H22']*Qtot['C10H22']))
C10H21O2_2 = DFT_energy['2-C10H21O2'] + ZPVE['2-C10H21O2'] - (kb*T*np.log(qvib['2-C10H21O2']/qvib['C10H22']*Qtot['C10H22']))
C10H21O2_3 = DFT_energy['3-C10H21O2'] + ZPVE['3-C10H21O2'] - (kb*T*np.log(qvib['3-C10H21O2']/qvib['C10H22']*Qtot['C10H22']))
C10H21O2_4 = DFT_energy['4-C10H21O2'] + ZPVE['4-C10H21O2'] - (kb*T*np.log(qvib['4-C10H21O2']/qvib['C10H22']*Qtot['C10H22']))
C10H21O2_5 = DFT_energy['5-C10H21O2'] + ZPVE['5-C10H21O2'] - (kb*T*np.log(qvib['5-C10H21O2']/qvib['C10H22']*Qtot['C10H22']))
C10H22 = DFT_energy['C10H22'] + ZPVE['C10H22'] - S_T['C10H22']
C9H19 = DFT_energy['C9H19'] + ZPVE['C9H19'] - (kb*T*np.log(qvib['C9H19']/qvib['C9H20']*Qtot['C9H20']))
C8H17 = DFT_energy['C8H17'] + ZPVE['C8H17'] - (kb*T*np.log(qvib['C8H17']/qvib['C8H18']*Qtot['C8H18']))
C7H15 = DFT_energy['C7H15'] + ZPVE['C7H15'] - (kb*T*np.log(qvib['C7H15']/qvib['C7H16']*Qtot['C7H16']))
C6H13 = DFT_energy['C6H13'] + ZPVE['C6H13'] - (kb*T*np.log(qvib['C6H13']/qvib['C6H14']*Qtot['C6H14']))
C5H11 = DFT_energy['C5H11'] + ZPVE['C5H11'] - (kb*T*np.log(qvib['C5H11']/qvib['C5H12']*Qtot['C5H12']))
C4H9 = DFT_energy['C4H9'] + ZPVE['C4H9'] - (kb*T*np.log(qvib['C4H9']/qvib['C4H10']*Qtot['C4H10']))
C3H7 = DFT_energy['C3H7'] + ZPVE['C3H7'] - (kb*T*np.log(qvib['C3H7']/qvib['C3H8']*Qtot['C3H8']))
C2H5 = DFT_energy['C2H5'] + ZPVE['C2H5'] - (kb*T*np.log(qvib['C2H5']/qvib['C2H6']*Qtot['C2H6']))
C2H4 = DFT_energy['C2H4'] + ZPVE['C2H4'] - S_T['C2H4']
CH3 = DFT_energy['CH3'] + ZPVE['CH3'] - S_T['CH3']
H = DFT_energy['H'] + ZPVE['H'] - S_T['H']
H2 = DFT_energy['H2'] + ZPVE['H2'] - S_T['H2']
H2O = DFT_energy['H2O'] + ZPVE['H2O'] - S_T['H2O']
HO2 = DFT_energy['HO2'] + ZPVE['HO2'] - S_T['HO2']
NO = DFT_energy['NO'] + ZPVE['NO'] - S_T['NO']
NO2 = DFT_energy['NO2'] + ZPVE['NO2'] - S_T['NO2']
O = DFT_energy['O'] + ZPVE['O'] - 161.059/96.485/1000*T
O2 = DFT_energy['O2'] + ZPVE['O2'] - S_T['O2']
OH = DFT_energy['OH'] + ZPVE['OH'] - S_T['OH']

print('Reaction 1a: ')
rxn1a = H + C10H21_1 - C10H22
rxn1a_std = np.std(np.array(DFT_std['H']) + np.array(DFT_std['1-C10H21']) - np.array(DFT_std['C10H22']))
print(rxn1a)
print(rxn1a_std)

print('Reaction 1b: ')
rxn1b = H + C10H21_2 - C10H22
rxn1b_std = np.std(np.array(DFT_std['H']) + np.array(DFT_std['2-C10H21']) - np.array(DFT_std['C10H22']))
print(rxn1b)
print(rxn1b_std)

print('Reaction 1c: ')
rxn1c = H + C10H21_3 - C10H22
rxn1c_std = np.std(np.array(DFT_std['H']) + np.array(DFT_std['3-C10H21']) - np.array(DFT_std['C10H22']))
print(rxn1c)
print(rxn1c_std)

print('Reaction 1d: ')
rxn1d = H + C10H21_4 - C10H22
rxn1d_std = np.std(np.array(DFT_std['H']) + np.array(DFT_std['4-C10H21']) - np.array(DFT_std['C10H22']))
print(rxn1d)
print(rxn1d_std)

print('Reaction 1e: ')
rxn1e = H + C10H21_5 - C10H22
rxn1e_std = np.std(np.array(DFT_std['H']) + np.array(DFT_std['5-C10H21']) - np.array(DFT_std['C10H22']))
print(rxn1e)
print(rxn1e_std)

print('Reaction 2: ')
rxn2 = C9H19 + CH3 - C10H22
rxn2_std = np.std(np.array(DFT_std['C9H19']) + np.array(DFT_std['CH3']) - np.array(DFT_std['C10H22']))
print(rxn2)
print(rxn2_std)

print('Reaction 3a: ')
rxn3a = HO2 + C10H21_1 - C10H22 - O2
rxn3a_std = np.std(np.array(DFT_std['HO2']) + np.array(DFT_std['1-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['O2']))
print(rxn3a)
print(rxn3a_std)

print('Reaction 3b: ')
rxn3b = HO2 + C10H21_2 - C10H22 - O2
rxn3b_std = np.std(np.array(DFT_std['HO2']) + np.array(DFT_std['2-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['O2']))
print(rxn3b)
print(rxn3b_std)

print('Reaction 3c: ')
rxn3c = HO2 + C10H21_3 - C10H22 - O2
rxn3c_std = np.std(np.array(DFT_std['HO2']) + np.array(DFT_std['3-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['O2']))
print(rxn3c)
print(rxn3c_std)

print('Reaction 3d: ')
rxn3d = HO2 + C10H21_4 - C10H22 - O2
rxn3d_std = np.std(np.array(DFT_std['HO2']) + np.array(DFT_std['4-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['O2']))
print(rxn3d)
print(rxn3d_std)

print('Reaction 3e: ')
rxn3e = HO2 + C10H21_5 - C10H22 - O2
rxn3e_std = np.std(np.array(DFT_std['HO2']) + np.array(DFT_std['5-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['O2']))
print(rxn3e)
print(rxn3e_std)

print('Reaction 4a: ')
rxn4a = H2 + C10H21_1 - C10H22 - H
rxn4a_std = np.std(np.array(DFT_std['H2']) + np.array(DFT_std['1-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['H']))
print(rxn4a)
print(rxn4a_std)

print('Reaction 4b: ')
rxn4b = H2 + C10H21_2 - C10H22 - H
rxn4b_std = np.std(np.array(DFT_std['H2']) + np.array(DFT_std['2-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['H']))
print(rxn4b)
print(rxn4b_std)

print('Reaction 4c: ')
rxn4c = H2 + C10H21_3 - C10H22 - H
rxn4c_std = np.std(np.array(DFT_std['H2']) + np.array(DFT_std['3-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['H']))
print(rxn4c)
print(rxn4c_std)

print('Reaction 4d: ')
rxn4d = H2 + C10H21_4 - C10H22 - H
rxn4d_std = np.std(np.array(DFT_std['H2']) + np.array(DFT_std['4-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['H']))
print(rxn4d)
print(rxn4d_std)

print('Reaction 4e: ')
rxn4e = H2 + C10H21_5 - C10H22 - H
rxn4e_std = np.std(np.array(DFT_std['H2']) + np.array(DFT_std['5-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['H']))
print(rxn4e)
print(rxn4e_std)

print('Reaction 5a: ')
rxn5a = H2O + C10H21_1 - C10H22 - OH
rxn5a_std = np.std(np.array(DFT_std['H2O']) + np.array(DFT_std['1-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['OH']))
print(rxn5a)
print(rxn5a_std)

print('Reaction 5b: ')
rxn5b = H2O + C10H21_2 - C10H22 - OH
rxn5b_std = np.std(np.array(DFT_std['H2O']) + np.array(DFT_std['2-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['OH']))
print(rxn5b)
print(rxn5b_std)

print('Reaction 5c: ')
rxn5c = H2O + C10H21_3 - C10H22 - OH
rxn5c_std = np.std(np.array(DFT_std['H2O']) + np.array(DFT_std['3-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['OH']))
print(rxn5c)
print(rxn5c_std)

print('Reaction 5d: ')
rxn5d = H2O + C10H21_4 - C10H22 - OH
rxn5d_std = np.std(np.array(DFT_std['H2O']) + np.array(DFT_std['4-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['OH']))
print(rxn5d)
print(rxn5d_std)

print('Reaction 5e: ')
rxn5e = H2O + C10H21_5 - C10H22 - OH
rxn5e_std = np.std(np.array(DFT_std['H2O']) + np.array(DFT_std['5-C10H21']) - np.array(DFT_std['C10H22']) - np.array(DFT_std['OH']))
print(rxn5e)
print(rxn5e_std)

print('Reaction 6a: ')
rxn6a = C10H21O2_1 - C10H21_1 - O2
rxn6a_std = np.std(np.array(DFT_std['1-C10H21O2']) - np.array(DFT_std['1-C10H21']) - np.array(DFT_std['O2']))
print(rxn6a)
print(rxn6a_std)

print('Reaction 6b: ')
rxn6b = C10H21O2_2 - C10H21_2 - O2
rxn6b_std = np.std(np.array(DFT_std['2-C10H21O2']) - np.array(DFT_std['2-C10H21']) - np.array(DFT_std['O2']))
print(rxn6b)
print(rxn6b_std)

print('Reaction 6c: ')
rxn6c = C10H21O2_3 - C10H21_3 - O2
rxn6c_std = np.std(np.array(DFT_std['3-C10H21O2']) - np.array(DFT_std['3-C10H21']) - np.array(DFT_std['O2']))
print(rxn6c)
print(rxn6c_std)

print('Reaction 6d: ')
rxn6d = C10H21O2_4 - C10H21_4 - O2
rxn6d_std = np.std(np.array(DFT_std['4-C10H21O2']) - np.array(DFT_std['4-C10H21']) - np.array(DFT_std['O2']))
print(rxn6d)
print(rxn6d_std)

print('Reaction 6e: ')
rxn6e = C10H21O2_5 - C10H21_5 - O2
rxn6e_std = np.std(np.array(DFT_std['5-C10H21O2']) - np.array(DFT_std['5-C10H21']) - np.array(DFT_std['O2']))
print(rxn6e)
print(rxn6e_std)

print('Reaction 7: ')
rxn7 = HO2 - H - O2
rxn7_std = np.std(np.array(DFT_std['HO2']) - np.array(DFT_std['H']) - np.array(DFT_std['O2']))
print(rxn7)
print(rxn7_std)

print('Reaction 8: ')
rxn8 = OH + O - O2 - H
rxn8_std = np.std(np.array(DFT_std['OH']) + np.array(DFT_std['O']) - np.array(DFT_std['O2']) - np.array(DFT_std['H']))
print(rxn8)
print(rxn8_std)

print('Reaction 9: ')
rxn9 = H2 - H - H
rxn9_std = np.std(np.array(DFT_std['H2']) - np.array(DFT_std['H']) - np.array(DFT_std['H']))
print(rxn9)
print(rxn9_std)

print('Reaction 10: ')
rxn10 = NO2 + OH - HO2 - NO
rxn10_std = np.std(np.array(DFT_std['NO2']) + np.array(DFT_std['OH']) - np.array(DFT_std['HO2']) - np.array(DFT_std['NO']))
print(rxn10)
print(rxn10_std)

print('Reaction 11a: ')
rxn11a = C10H21O_1 + NO2 - C10H21O2_1 - NO
rxn11a_std = np.std(np.array(DFT_std['1-C10H21O']) + np.array(DFT_std['NO2']) - np.array(DFT_std['1-C10H21O2']) - np.array(DFT_std['NO']))
print(rxn11a)
print(rxn11a_std)

print('Reaction 11b: ')
rxn11b = C10H21O_2 + NO2 - C10H21O2_2 - NO
rxn11b_std = np.std(np.array(DFT_std['2-C10H21O']) + np.array(DFT_std['NO2']) - np.array(DFT_std['2-C10H21O2']) - np.array(DFT_std['NO']))
print(rxn11b)
print(rxn11b_std)

print('Reaction 11c: ')
rxn11c = C10H21O_3 + NO2 - C10H21O2_3 - NO
rxn11c_std = np.std(np.array(DFT_std['3-C10H21O']) + np.array(DFT_std['NO2']) - np.array(DFT_std['3-C10H21O2']) - np.array(DFT_std['NO']))
print(rxn11c)
print(rxn11c_std)

print('Reaction 11d: ')
rxn11d = C10H21O_4 + NO2 - C10H21O2_4 - NO
rxn11d_std = np.std(np.array(DFT_std['4-C10H21O']) + np.array(DFT_std['NO2']) - np.array(DFT_std['4-C10H21O2']) - np.array(DFT_std['NO']))
print(rxn11d)
print(rxn11d_std)

print('Reaction 11e: ')
rxn11e = C10H21O_5 + NO2 - C10H21O2_5 - NO
rxn11e_std = np.std(np.array(DFT_std['5-C10H21O']) + np.array(DFT_std['NO2']) - np.array(DFT_std['5-C10H21O2']) - np.array(DFT_std['NO']))
print(rxn11e)
print(rxn11e_std)

print('Reaction 12: ')
rxn12 = NO2 + NO2 - O2 - NO - NO
rxn12_std = np.std(np.array(DFT_std['NO2']) + np.array(DFT_std['NO2']) - np.array(DFT_std['O2']) - np.array(DFT_std['NO']) - np.array(DFT_std['NO']))
print(rxn12)
print(rxn12_std)

print('Reaction 13: ')
rxn13 = C8H17 + C2H5 - C10H22
rxn13_std = np.std(np.array(DFT_std['C8H17']) + np.array(DFT_std['C2H5']) - np.array(DFT_std['C10H22']))
print(rxn13)
print(rxn13_std)

print('Reaction 14: ')
rxn14 = C7H15 + C3H7 - C10H22
rxn14_std = np.std(np.array(DFT_std['C7H15']) + np.array(DFT_std['C3H7']) - np.array(DFT_std['C10H22']))
print(rxn14)
print(rxn14_std)

print('Reaction 15: ')
rxn15 = C6H13 + C4H9 - C10H22
rxn15_std = np.std(np.array(DFT_std['C6H13']) + np.array(DFT_std['C4H9']) - np.array(DFT_std['C10H22']))
print(rxn15)
print(rxn15_std)

print('Reaction 16: ')
rxn16 = C5H11 + C5H11 - C10H22
rxn16_std = np.std(np.array(DFT_std['C5H11']) + np.array(DFT_std['C5H11']) - np.array(DFT_std['C10H22']))
print(rxn16)
print(rxn16_std)

print('Reaction 17: ')
rxn17 = H + C2H4 - C2H5
rxn17_std = np.std(np.array(DFT_std['H']) + np.array(DFT_std['C2H4']) - np.array(DFT_std['C2H5']))
print(rxn17)
print(rxn17_std)

rxns = [['rxn1a',rxn1a,rxn1a_std],['rxn1b',rxn1b,rxn1b_std],['rxn1c',rxn1c,rxn1c_std],['rxn1d',rxn1d,rxn1d_std],['rxn1e',rxn1e,rxn1e_std],['rxn2',rxn2,rxn2_std],['rxn3a',rxn3a,rxn3a_std],['rxn3b',rxn3b,rxn3b_std],['rxn3c',rxn3c,rxn3c_std],['rxn3d',rxn3d,rxn3d_std],['rxn3e',rxn3e,rxn3e_std],['rxn4a',rxn4a,rxn4a_std],['rxn4b',rxn4b,rxn4b_std],['rxn4c',rxn4c,rxn4c_std],['rxn4d',rxn4d,rxn4d_std],['rxn4e',rxn4e,rxn4e_std],['rxn5a',rxn5a,rxn5a_std],['rxn5b',rxn5b,rxn5b_std],['rxn5c',rxn5c,rxn5c_std],['rxn5d',rxn5d,rxn5d_std],['rxn5e',rxn5e,rxn5e_std],['rxn6a',rxn6a,rxn6a_std],['rxn6b',rxn6b,rxn6b_std],['rxn6c',rxn6c,rxn6c_std],['rxn6d',rxn6d,rxn6d_std],['rxn6e',rxn6e,rxn6e_std],['rxn7',rxn7,rxn7_std],['rxn8',rxn8,rxn8_std],['rxn9',rxn9,rxn9_std],['rxn10',rxn10,rxn10_std],['rxn11a',rxn11a,rxn11a_std],['rxn11b',rxn11b,rxn11b_std],['rxn11c',rxn11c,rxn11c_std],['rxn11d',rxn11d,rxn11d_std],['rxn11e',rxn11e,rxn11e_std],['rxn12',rxn12,rxn12_std],['rxn13',rxn13,rxn13_std],['rxn14',rxn14,rxn14_std],['rxn15',rxn15,rxn15_std],['rxn16',rxn16,rxn16_std],['rxn17',rxn17,rxn17_std]]

with open('Reaction_Energies.csv', 'w+') as g:
    for row in rxns:
        for column in row:
            g.write('%s;' % column)
        g.write('\n')
