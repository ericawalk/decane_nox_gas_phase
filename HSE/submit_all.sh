#!/bin/sh
for d in */
do
    cd $d
    sbatch slurm_run_vasp.sh
    echo "$d submitted"
    cd ../
done
